#
# Copyright (C) 2025 The Android Open Source Project
# Copyright (C) 2025 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_fogos.mk

COMMON_LUNCH_CHOICES := \
    omni_fogos-user \
    omni_fogos-userdebug \
    omni_fogos-eng
